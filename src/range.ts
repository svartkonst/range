/**
 * 
 */
export function range(from: number): Iterable<number> & Iterator<number> {
    return {
        [Symbol.iterator](): Iterable<number> & Iterator<number> {
            return this;
        }, 
        next(): IteratorResult<number> {
            return { done: true, value: from };
        }
    }
}