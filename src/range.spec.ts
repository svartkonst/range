import { range } from './range';

for (const i of range(1, 5)) {
    console.error(i)
}

describe('Range', () => {
 it('should be a function', () => {
    expect(range).toBeInstanceOf(Function);
 });

 it('should have length 2', () => {
    expect(range.length).toBe(2);
 });
});